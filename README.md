# Solve character matrix

This Project aims to solve character matrices automatically and display all words that are contained in them. 
This is an early prototype that still needs some manuel work to be done in order to solve a character matrix. 

There is no image recognition/OCR implemented yet. 
If you don't have your character matrix in text form you need to use an online tool like this https://www.onlineocr.net/de/ or write off the text manually. 
