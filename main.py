import os
import sys

def main():
    dictionary_german = read_dictionary(os.path.join(sys.path[0], "resources", "Wörterbuch_Deutsch.txt"));
    dictionary_german = remove_short_words_from_list(dictionary_german, 4)
    dictionary_german = convert_to_uppercase(dictionary_german)

    dictionary_english = read_dictionary(os.path.join(sys.path[0], "resources", "Wörterbuch_Englisch.txt"));
    dictionary_english = remove_short_words_from_list(dictionary_english, 4)
    dictionary_english = convert_to_uppercase(dictionary_english)

    # save dictionary as new file
    #with open("C:/Users/YannicB/Documents/DeutschesWörterbuch_ohne_kurze_Wörter.txt", "w+") as new_dictionary_file:
    #    for i in range(len(dictionary) - 1):
    #        new_dictionary_file.write(dictionary[i])


    # Enter the character matrix you want to be soved here. Each line should be seperated by a semicolon
    text = "ZIQPUKQYZATSUSGLYR;CWGCNARJTLRZZKLHMP;XZLUTTSYLQAVNSEECE;JOYVEEREIGNISXHOOZ;JHPHRPWZJTSBAVIIBM;IBFZZTHGJYISNAXXJB;OPZUUWUCIASKFTZGEF;XVJWSVZBZZTUAEYUKI;UWAMTMTGVLIJNKZUTB;JVQTABQAULOMGMVCOT;OQVONHUWRINWSBBPGD;UUQDDSUJJNENZELSVD;KGONOZUSTANDUIRIYM;MPSZTYXOFRIMSPHGKG;CPUGKMCENTRYTIJNXS;ZYYUUWHIEXITAHOAMF;GUSLNUDPVOMINOBLOY;FEUEVHTGPSRIDNNQVO"
    text = text.upper()
    text_list = text.split(";")

    for i in range(len(text_list)):
        print("Line %d:" % i)
        find_words(text_list[i], dictionary_german)
        find_words(text_list[i], dictionary_english)
    text_list = mirror_text(text, ";")
    for i in range(len(text_list)):
        print("Column %d:" % i)
        find_words(text_list[i], dictionary_german)
        find_words(text_list[i], dictionary_english)


def read_dictionary(filepath):
    with open(filepath, "r") as dictionary_file:
        german_dictionary = dictionary_file.read().split("\n")
        return german_dictionary


def remove_short_words_from_list(input_list, max_length):
    # Remove words with less then max_length characters (including the \n character)
    current_word = 0
    total_word_count = len(input_list)
    while current_word < total_word_count:
        if len(input_list[current_word]) < max_length:
            del input_list[current_word]
            total_word_count -= 1
        else:
            current_word += 1

    return input_list


def convert_to_uppercase(input_text):
    for i in range(len(input_text)):
        input_text[i] = input_text[i].upper()
    return input_text


def mirror_text(input_text, line_end_character="\n"):
    lines = input_text.split(line_end_character)
    new_lines = []
    for x in range(len(lines[0])):
        new_lines.append("")
        for y in range(len(lines)):
            new_lines[x] += lines[y][x]

    return new_lines


def find_words(input_text, dictionary):
    for i in range(len(dictionary)):
        index = input_text.find(dictionary[i])
        if index != -1:
            print("[" + dictionary[i] + "]" + " found at index " + str(index))


main()
